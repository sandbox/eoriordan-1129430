<?php 

/**
 * Implementation of hook_permission().
 */
function createpage_ui_permission() {
  return array(
    'administer createpage' => array(
      'title' => t('Administer Custom Pages'),
      'description' => t('Allow user to add and remove custom pages'),
    )
  );
}

function createpage_ui_menu() {

  $access = array('administer createpage');

  $items = array();
  $items['admin/structure/createpage'] = array(
    'title' => 'List Custom Pages',
    'page callback' => 'createpage_ui_admin_settings',
    'access arguments' => $access,
    'weight' => -10,
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/structure/createpage/list'] = array(
    'title' => 'List Custom Pages',
    'page callback' => 'createpage_ui_admin_settings',
    'access arguments' => $access,
    'weight' => -10,
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  $items['admin/structure/createpage/add'] = array(
    'title' => 'Add a Custom Page',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('createpage_ui_admin_settings_form'),
    'access arguments' => $access,
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/structure/createpage/edit/%createpage'] = array(
    'title' => 'Edit Custom Page',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('createpage_ui_admin_settings_form', 4),
    'access arguments' => $access,
    'type' => MENU_CALLBACK,
  );
  $items['admin/structure/createpage/delete/%createpage'] = array(
    'title' => 'Delete Page',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('createpage_ui_delete_form', 4),
    'access arguments' => $access,
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

function createpage_load($key) {
  $settings = variable_get('CREATEPAGE_UI_CONFIG', FALSE );

  if ( $settings == FALSE ) {
    return FALSE;
  }

  if ( is_object( $settings[$key] )) {
    return $settings[$key];
  }
  else {
    return FALSE;
  }
}

function createpage_ui_admin_settings() {
  $settings = $settings = variable_get('CREATEPAGE_UI_CONFIG', array());

  $out = "
<table>
  <tr>
    <th>" . t("Title") . "</th>
    <th>" . t("Key") . "</th>
    <th>" . t("Path") . "</th>
    <th colspan=\"2\" align=\"center\">" . t("Control") . "</th>
  </tr>
  ";

  foreach ( $settings as $obj ) {
    $options = array('attributes' => array('target' => '_blank'));
    $url = l($obj->path, $obj->path, $options);

    $out .= "<tr>";
    $out .= "<td>" . $obj->title . "</td>";
    $out .= "<td>" . $obj->key . "</td>";
    $out .= "<td>" . $url . "</td>";
    $out .= "<td>" . l(t('Edit'), 'admin/structure/createpage/edit/' . $obj->key ) . "</td>";
    $out .= "<td>" . l(t('Delete'), 'admin/structure/createpage/delete/' . $obj->key) . "</td>";
    $out .= "</tr>";
  }

  $out .= "</table>";

  return $out;
}

function createpage_ui_admin_settings_form($node, &$form_state, $saved = null) {
  if ( is_null($saved) ) {
  //$saved = ( is_null($saved) ) ? new stdClass() : $saved;
    $saved = new stdClass();
    $saved->title = '';
    $saved->key = '';
    $saved->path = '';
    $saved->menu = '';
  }

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $saved->title,
    '#size' => 50,
    '#maxlength' => 50,
    '#description' => t('Title of the page.'),
  );
  
  $form['key'] = array(
    '#type' => 'textfield',
    '#title' => t('Key'),
    '#default_value' => $saved->key,
    '#required' => TRUE,
    '#size' => 50,
    '#maxlength' => 50,
    '#description' => t('Please indicate a unique key for this page.'),
  );

  $form['path'] = array(
    '#type' => 'textfield',
    '#title' => t('URI Path'),
    '#default_value' => $saved->path,
    '#required' => FALSE,
    '#size' => 50,
    '#maxlength' => 50,
    '#description' => t('The URL.'),
  );

  $default_menu = empty($saved->menu) ? 'main-menu' : $saved->menu;
  $options = menu_get_menus();
  $form['menu'] = array(
    '#type' => 'select',
    '#title' => t('Menu'),
    '#default_value' => $default_menu,
    '#options' => $options,
    '#required' => FALSE,
    '#description' => t('The parent Menu.'),
    //'#attributes' => array('class' => array('menu-title-select')),
  );

  if ( trim($saved->key) != "" ) {
    drupal_set_title(t('Edit page: %title', $args = array('%title' => $saved->title)), $output = PASS_THROUGH);
    $form['save'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
      '#submit' => array('createpage_ui_settings_submit'),
    );
  }
  else {
    drupal_set_title(t('Add Page'), $output = CHECK_PLAIN);
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add Component'),
      '#submit' => array('createpage_ui_settings_submit'),
    );
  }

  return $form;
}

function createpage_ui_settings_submit( $form, &$form_state ) {
  $post = $form_state['values'];
  $obj = new stdClass();
  $obj->title = $post['title'];
  $obj->key = $post['key'];
  $obj->path = $post['path'];
  $obj->menu = $post['menu'];
  $settings = variable_get('CREATEPAGE_UI_CONFIG', array() );
  $settings[$obj->key] = $obj;
  variable_set('CREATEPAGE_UI_CONFIG', $settings);
  drupal_flush_all_caches();
  //drupal_goto('admin/structure/createpage');
}

/*
  Delete
*/
function createpage_ui_delete_form($node, &$form_state, $setting) {
  $form['title'] = array(
    '#type' => 'hidden',
    '#value' => $setting->title,
  );
  $form['key'] = array(
    '#type' => 'hidden',
    '#value' => $setting->key,
  );

  return confirm_form(
    $form,
    t('Delete page %title', array('%title' => $setting->title)),
    'admin/structure/createpage',
    '<p>' . t('Are you sure you want to delete the custom page %title?', array('%title' => $setting->title)) . '</p>',
    t('Delete'),
    t('Cancel')
  );
}
function createpage_ui_delete_form_submit($form, &$form_state) {
  $key = $form_state['values']['key'];
  $settings = variable_get('CREATEPAGE_UI_CONFIG', null );
  if ( is_array( $settings ) && is_object( $settings[$key] ) ) {
    $new_settings = array_diff_key($settings, array($key => 'remove') );
  }
  variable_set( 'CREATEPAGE_UI_CONFIG', $new_settings );
  drupal_flush_all_caches();
  drupal_set_message(t('The custom page: %title has been deleted.', array('%title' => $form_state['values']['title'])));
  $form_state['redirect'] = 'admin/structure/createpage';
  return;
}

/**
 * Implementation of hook_createpages
 */
function createpage_ui_createpages() {
  return variable_get( 'CREATEPAGE_UI_CONFIG', null );
}