<?php
/**
 * Expose node views/node forms of specific node types as a context condition.
 */
class createpage_context_condition_alias extends context_condition {

  function condition_values() {
    $values = _createpage_context_get_custompages();
    return $values;
  }

  function execute($path_key) {
    if (empty($path_key)) {
      return;
    }

    foreach (context_enabled_contexts() as $context) {
      if ($paths = $this->fetch_from_context($context, 'values')) {
        if (in_array($path_key, $paths)) {
          $this->condition_met($context, $path_key);
        }
      }
    }

  }
}
